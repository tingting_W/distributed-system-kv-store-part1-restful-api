from flask import Flask, abort, request

app = Flask(__name__)

@app.route('/hello', methods=['GET'])
def hello():
	return 'Hello world!'

@app.route('/echo', methods=['GET'])	
def echo():
	return request.args.get('msg', '')

@app.route('/', methods=['POST'])
def post():
	print('Error!')
	return abort(404)	

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8080)